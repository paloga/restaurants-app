package com.paloga.restaurantsapp

import android.support.test.espresso.Espresso.onView
import android.support.test.espresso.action.ViewActions.*
import android.support.test.espresso.matcher.ViewMatchers.withId
import android.support.test.rule.ActivityTestRule
import android.support.test.runner.AndroidJUnit4
import com.paloga.restaurantsapp.CustomAssertions.Companion.hasItemCount
import com.paloga.restaurantsapp.presentation.RestaurantsActivity
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class RestaurantsTest {
    @Rule
    @JvmField
    val activity = ActivityTestRule<RestaurantsActivity>(RestaurantsActivity::class.java)

    @Test
    fun countRestaurants() {
        onView(withId(R.id.edtPostCode)).perform(clearText())
        onView(withId(R.id.edtPostCode)).perform(typeText("se19"), closeSoftKeyboard());
        onView(withId(R.id.btnSearch)).perform(click())

        Thread.sleep(5000) // I know it is not the best solution but, I need to improve my testing knowledge.

        onView(withId(R.id.contentView)).check(hasItemCount(248))

    }
}
