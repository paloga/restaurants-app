package com.paloga.restaurantsapp.commons.extensions

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.support.annotation.StringRes
import android.support.design.widget.Snackbar
import android.support.v4.app.ActivityOptionsCompat
import android.support.v4.view.ViewCompat
import android.view.View
import android.widget.Toast


fun Activity.openActivity(javaClass: Class<*>) = startActivity(Intent(this, javaClass))

fun Activity.openActivity(javaClass: Class<*>, params: Bundle) =
        startActivity(Intent(this, javaClass).putExtras(params))

fun Activity.openActivityForResult(javaClass: Class<*>, params: Bundle, requestCode: Int) =
        startActivityForResult(Intent(this, javaClass).putExtras(params), requestCode)

fun Activity.openActivityForResult(javaClass: Class<*>, requestCode: Int) =
        startActivityForResult(Intent(this, javaClass), requestCode)

fun Activity.openActivityWithSharedTransitionPhoto(javaClass: Class<*>, params: Bundle, sharedElement: View) {
    val transitionName = ViewCompat.getTransitionName(sharedElement)
    val options = ActivityOptionsCompat.makeSceneTransitionAnimation(
            this,
            sharedElement, transitionName)
    params.putString("user_photo_transition", transitionName)
    startActivity(Intent(this, javaClass).putExtras(params), options.toBundle())
}

fun Activity.toast(message: CharSequence, duration: Int = Toast.LENGTH_SHORT) {
    Toast.makeText(this, message, duration).show()
}

fun Activity.toast(@StringRes messageRes: Int, duration: Int = Toast.LENGTH_SHORT) {
    toast(getString(messageRes), duration)
}

fun Activity.snackbar(view: View, @StringRes messageRes: Int, duration: Int = Snackbar.LENGTH_SHORT) {
    Snackbar.make(view, messageRes, duration).show()
}
