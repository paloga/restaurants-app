package com.paloga.restaurantsapp.commons.extensions

import android.widget.ImageView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.paloga.restaurantsapp.R

fun ImageView.loadRoundedImage(imageUrl: String) {
    Glide.with(this.context)
            .load(imageUrl)
            .apply(RequestOptions.circleCropTransform().placeholder(R.drawable.ic_launcher))
            .into(this)
}

