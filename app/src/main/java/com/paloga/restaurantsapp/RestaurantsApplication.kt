package com.paloga.restaurantsapp

import android.app.Application
import timber.log.Timber

class RestaurantsApplication : Application() {

    override fun onCreate() {
        super.onCreate()

        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        }
    }
}