package com.paloga.restaurantsapp.permissions

import android.app.Activity
import android.support.v4.app.ActivityCompat

object PermissionChecker {

    fun withActivity(activity: Activity) = ActivityBuilder(activity)

    class ActivityBuilder(private val activity: Activity) {
        fun withPermission(permission: String) = PermissionBuilder(activity, permission)
    }

    class PermissionBuilder(private val activity: Activity, private val permission: String) {
        fun withListener(listener: PermissionAskListener) = PermissionListenerBuilder(activity, permission, listener)
    }

    class PermissionListenerBuilder(private val activity: Activity, private val permission: String, private val listener: PermissionAskListener) {
        fun check() = PermissionUtils.checkPermission(activity, permission, listener)
    }

    fun requestPermission(activity: Activity, permission: String) {
        ActivityCompat.requestPermissions(activity, arrayOf(permission), 0)

    }
}