package com.paloga.restaurantsapp.permissions

import android.app.Activity
import android.content.Context
import android.content.pm.PackageManager
import android.support.v4.app.ActivityCompat
import com.paloga.restaurantsapp.permissions.PermissionUtils.PermissionPreferencesKey.FIRST_TIME_ASK_PREFIX
import com.paloga.restaurantsapp.shared_preferences.AndroidPrefsManager
import com.paloga.restaurantsapp.shared_preferences.PreferencesManager


internal object PermissionUtils {

    internal object PermissionPreferencesKey {
        const val FIRST_TIME_ASK_PREFIX = "first_time_"
    }

    fun isPermissionGranted(context: Context, permission: String): Boolean {
        val permissionResult = ActivityCompat.checkSelfPermission(context, permission)
        return permissionResult == PackageManager.PERMISSION_GRANTED
    }

    internal fun checkPermission(activity: Activity, permission: String, listener: PermissionAskListener) {
        /*
        * If permission is not granted
        * */
        if (!isPermissionGranted(activity, permission)) {
            /*
            * If permission denied previously
            * */
            if (ActivityCompat.shouldShowRequestPermissionRationale(activity, permission)) {
                listener.onPermissionPreviouslyDenied(permission)
            } else {
                val preferences = AndroidPrefsManager(activity)
                /*
                * Permission denied or first time requested
                * */
                if (isFirstTimeAskingPermission(preferences, permission)) {
                    saveFirstTimeAskingPermission(preferences, permission)
                    listener.onNeedPermission(permission)
                } else {
                    /*
                    * Handle the feature without permission or ask user to manually allow permission
                    * */
                    listener.onPermissionDisabled(permission)
                }
            }
        } else {
            listener.onPermissionGranted(permission)
        }
    }

    private fun isFirstTimeAskingPermission(preferences: PreferencesManager, permission: String) =
            preferences.getBooleanPreference(FIRST_TIME_ASK_PREFIX + permission, default = true)

    private fun saveFirstTimeAskingPermission(preferences: PreferencesManager, permission: String) =
            preferences.saveBooleanPreference(FIRST_TIME_ASK_PREFIX + permission, false)

}