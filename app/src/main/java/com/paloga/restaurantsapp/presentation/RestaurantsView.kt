package com.paloga.restaurantsapp.presentation

import com.hannesdorfmann.mosby3.mvp.lce.MvpLceView
import com.paloga.restaurantsapp.domain.model.Restaurant

interface RestaurantsView : MvpLceView<List<Restaurant>> {

    fun initViews()

    fun setCurrentPostCode(postCode: String)

    fun updateRestaurantsCounter(restaurantsCount: Int)

    fun showStartView()

    fun showEmptyView()

    fun showPostCodeEmptyError()

    fun checkLocationPermission()

}