package com.paloga.restaurantsapp.presentation

import com.hannesdorfmann.mosby3.mvp.MvpBasePresenter
import com.paloga.restaurantsapp.domain.model.Restaurant
import com.paloga.restaurantsapp.domain.usecase.GetCurrentPostCode
import com.paloga.restaurantsapp.domain.usecase.GetRestaurants
import timber.log.Timber

class RestaurantsPresenter(private val getRestaurants: GetRestaurants,
                           private val getCurrentPostCode: GetCurrentPostCode) : MvpBasePresenter<RestaurantsView>() {

    fun start() {
        ifViewAttached {
            it.initViews()
            it.showStartView()
            it.checkLocationPermission()
        }
    }

    fun onLocationPermissionGranted() {
        loadCurrentPostCode()
    }

    private fun loadCurrentPostCode() {
        getCurrentPostCode.getPostCode { onPostCodeLoaded(it) }
    }

    private fun onPostCodeLoaded(postCode: String) {
        ifViewAttached {
            it.setCurrentPostCode(postCode)
            it.showLoading(false)
            it.loadData(false)
        }
    }

    fun onSearchClicked(postCode: String) {
        ifViewAttached {
            if (postCode.isEmpty()) {
                it.showPostCodeEmptyError()
            }else {
                it.showLoading(false)
                it.loadData(false)
            }
        }
    }

    fun loadRestaurants(postCode: String) {
        Timber.d("Loading restaurants for postCode $postCode...")
        getRestaurants.getRestaurantsByPostCode(postCode) { onRestaurantsLoaded(it) }
    }

    private fun onRestaurantsLoaded(restaurants: List<Restaurant>) {
        Timber.d("onRestaurantsLoaded: ${restaurants.size} restaurants")
        ifViewAttached {
            if (restaurants.isNotEmpty()) {
                it.setData(restaurants)
                it.showContent()
                it.updateRestaurantsCounter(restaurants.size)
            } else {
                it.showEmptyView()
            }
        }
    }
}