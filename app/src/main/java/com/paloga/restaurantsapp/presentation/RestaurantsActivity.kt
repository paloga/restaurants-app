package com.paloga.restaurantsapp.presentation

import android.Manifest
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.Toolbar
import android.view.MenuItem
import com.afollestad.materialdialogs.MaterialDialog
import com.hannesdorfmann.mosby3.mvp.lce.MvpLceActivity
import com.paloga.restaurantsapp.R
import com.paloga.restaurantsapp.commons.extensions.gone
import com.paloga.restaurantsapp.commons.extensions.hideKeyboard
import com.paloga.restaurantsapp.commons.extensions.snackbar
import com.paloga.restaurantsapp.commons.extensions.visible
import com.paloga.restaurantsapp.data.datasource.AndroidLocationDataSource
import com.paloga.restaurantsapp.data.datasource.RestaurantsDataFactory
import com.paloga.restaurantsapp.data.repository.RestaurantsDataRepository
import com.paloga.restaurantsapp.domain.model.Restaurant
import com.paloga.restaurantsapp.domain.usecase.GetCurrentPostCode
import com.paloga.restaurantsapp.domain.usecase.GetRestaurants
import com.paloga.restaurantsapp.permissions.PermissionAskListener
import com.paloga.restaurantsapp.permissions.PermissionChecker
import com.paloga.restaurantsapp.permissions.PermissionUtils
import kotlinx.android.synthetic.main.activity_restaurants.*
import kotlinx.android.synthetic.main.notification_template_lines_media.view.*
import kotlinx.android.synthetic.main.toolbar.*
import org.jetbrains.anko.inputMethodManager
import timber.log.Timber


class RestaurantsActivity : MvpLceActivity<RecyclerView, List<Restaurant>, RestaurantsView, RestaurantsPresenter>(), RestaurantsView {

    private val restaurantsAdapter by lazy {
        RestaurantsAdapter(emptyList())
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_restaurants)

        presenter.start()
    }

    override fun setData(restaurantList: List<Restaurant>) {
        runOnUiThread {
            showLoading(false)
            restaurantsAdapter.restaurantList = restaurantList
        }
    }

    override fun loadData(pullToRefresh: Boolean) {
        presenter.loadRestaurants(edtPostCode.text.toString())
    }

    override fun createPresenter() = RestaurantsPresenter(GetRestaurants(RestaurantsDataRepository(RestaurantsDataFactory())), GetCurrentPostCode(AndroidLocationDataSource(this)))

    override fun getErrorMessage(e: Throwable?, pullToRefresh: Boolean): String = getString(R.string.restaurant_error)

    override fun initViews() {
        initToolbar()
        initRecyclerView()
        btnSearch.setOnClickListener {
            presenter.onSearchClicked(edtPostCode.text.toString())
            edtPostCode.hideKeyboard(inputMethodManager)
        }
    }

    private fun initToolbar() {
        val toolbar = toolbar as Toolbar?
        setSupportActionBar(toolbar)
        supportActionBar?.title = null
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        toolbar?.setNavigationIcon(R.drawable.ic_close)
    }

    private fun initRecyclerView() {
        contentView.layoutManager = LinearLayoutManager(this)
        contentView.adapter = restaurantsAdapter
        // contentView.itemAnimator = TODO [Paloga] Add item animator
    }

    override fun setCurrentPostCode(postCode: String) {
        edtPostCode.setText(postCode)
    }

    override fun updateRestaurantsCounter(restaurantsCount: Int) {
        tsCounter.setCurrentText(tsCounter.text?.toString())
        tsCounter.setText(restaurantsCount.toString())
    }

    override fun showStartView() {
        startView.visible()
        loadingView.gone()
        errorView.gone()
        contentView.gone()
        emptyView.gone()
    }

    override fun showEmptyView() {
        startView.gone()
        loadingView.gone()
        errorView.gone()
        contentView.gone()
        emptyView.visible()
    }

    override fun showLoading(pullToRefresh: Boolean) {
        super.showLoading(pullToRefresh)
        startView.gone()
        errorView.gone()
        contentView.gone()
        emptyView.gone()
    }

    override fun showContent() {
        super.showContent()
        startView.gone()
        emptyView.gone()
        errorView.gone()
        loadingView.gone()
    }

    override fun showPostCodeEmptyError() {
        snackbar(contentView, R.string.restaurant_error_empty_postcode)
    }

    override fun checkLocationPermission() {
        PermissionChecker
                .withActivity(this)
                .withPermission(Manifest.permission.ACCESS_FINE_LOCATION)
                .withListener(PermissionListener()).check()
    }

    private fun showLocationPermissionRationale(permission: String) {
        MaterialDialog.Builder(this)
                .title(R.string.permissions_location_dialog_title)
                .content(R.string.permissions_location_dialog_content)
                .positiveText(R.string.permissions_location_dialog_ok)
                .cancelable(false)
                .onPositive { _, _ -> PermissionChecker.requestPermission(this@RestaurantsActivity, permission) }
                .show()
    }

    inner class PermissionListener : PermissionAskListener {
        override fun onNeedPermission(permission: String) {
            Timber.d("Check permission - onNeedPermission %s", permission)
            showLocationPermissionRationale(permission)
        }

        override fun onPermissionPreviouslyDenied(permission: String) {
            Timber.d("Check permission - onPermissionPreviouslyDenied %s", permission)
        }

        override fun onPermissionDisabled(permission: String) {
            Timber.d("check permission - onPermissionDisabled %s", permission)
        }

        override fun onPermissionGranted(permission: String) {
            Timber.d("Check permission - onPermissionGranted %s", permission)
            presenter.onLocationPermissionGranted()
        }

        override fun onPermissionNotOSCompatible(permission: String) {
            Timber.d("Check permission - onPermissionNotOSCompatible %s", permission)
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        if (PermissionUtils.isPermissionGranted(this, Manifest.permission.ACCESS_FINE_LOCATION)) {
            presenter.onLocationPermissionGranted()
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                finish()
                return true
            }
        }

        return super.onOptionsItemSelected(item)
    }
}
