package com.paloga.restaurantsapp.presentation

import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import com.paloga.restaurantsapp.R
import com.paloga.restaurantsapp.commons.extensions.getLayoutInflater
import com.paloga.restaurantsapp.commons.extensions.loadRoundedImage
import com.paloga.restaurantsapp.domain.model.Restaurant
import kotlinx.android.synthetic.main.row_restaurant.view.*

class RestaurantsAdapter(var restaurantList: List<Restaurant>) : RecyclerView.Adapter<RestaurantsAdapter.RestaurantViewHolder>() {

    inner class RestaurantViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bindRestaurant(restaurant: Restaurant) = with(itemView) {
            restaurant.logo?.let { ivRestaurantPhoto.loadRoundedImage(it) }
            txtName.text = restaurant.name
            txtCuisineTypes.text = restaurant.cuisineTypes.joinToString(separator = ", ") { it.name }
            srbRating.rating = restaurant.rating
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RestaurantViewHolder {
        val rowView = parent.getLayoutInflater().inflate(R.layout.row_restaurant, parent, false)
        return RestaurantViewHolder(rowView)
    }

    override fun getItemCount(): Int = restaurantList.size

    override fun onBindViewHolder(holder: RestaurantViewHolder, position: Int) {
        holder.bindRestaurant(restaurantList[position])
    }
}