package com.paloga.restaurantsapp.domain.repository

import com.paloga.restaurantsapp.domain.model.RestaurantReadable

interface RestaurantsRepository {
    fun getRestaurantsByPostCode(postCode: String): List<RestaurantReadable>
}