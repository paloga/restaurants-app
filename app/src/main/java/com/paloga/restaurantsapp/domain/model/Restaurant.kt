package com.paloga.restaurantsapp.domain.model

data class Restaurant(override val name: String,
                      override val cuisineTypes: List<CuisineType>,
                      override val rating: Float,
                      override val logo: String?) : RestaurantReadable {
}