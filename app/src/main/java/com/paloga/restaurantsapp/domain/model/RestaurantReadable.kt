package com.paloga.restaurantsapp.domain.model

interface RestaurantReadable {
    val name: String
    val cuisineTypes: List<CuisineTypeReadable>
    val rating: Float
    val logo: String?
}