package com.paloga.restaurantsapp.domain.model

interface CuisineTypeReadable {
    val name: String
}