package com.paloga.restaurantsapp.domain.model

data class CuisineType(override val name: String) : CuisineTypeReadable {
}