package com.paloga.restaurantsapp.domain.usecase

import com.paloga.restaurantsapp.data.mapper.DbDataMapper
import com.paloga.restaurantsapp.domain.model.Restaurant
import com.paloga.restaurantsapp.domain.repository.RestaurantsRepository
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread

class GetRestaurants(private val restaurantsRepository: RestaurantsRepository) {

    fun getRestaurantsByPostCode(postCode: String, onRestaurantsLoaded: (List<Restaurant>) -> Unit) {
        doAsync {
            val restaurantsList = loadRestaurants(postCode)
            uiThread { onRestaurantsLoaded.invoke(restaurantsList) }
        }
    }

    private fun loadRestaurants(postCode: String): List<Restaurant> {
        val dbDataMapper = DbDataMapper()
        return restaurantsRepository.getRestaurantsByPostCode(postCode).map { dbDataMapper.convertToDomain(it) }.sortedBy { it.name }
    }
}