package com.paloga.restaurantsapp.domain.usecase

import com.paloga.restaurantsapp.data.datasource.LocationDataSource
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread

class GetCurrentPostCode(private val locationDataSource: LocationDataSource) {

    fun getPostCode(onPostCodeLoaded: (String) -> Unit) {
        doAsync {
            val postCode = loadPostCode()
            uiThread { onPostCodeLoaded.invoke(postCode) }
        }
    }

    private fun loadPostCode(): String {
        return locationDataSource.getPostCode()
    }
}