package com.paloga.restaurantsapp.data.entity

import com.google.gson.annotations.SerializedName

data class RestaurantResponse (@SerializedName("Name") val name: String,
                               @SerializedName("CuisineTypes") val cuisineTypes: List<CuisineTypeResponse>,
                               @SerializedName("RatingStars") val rating: Float,
                               @SerializedName("Logo") val logos: List<LogoResponse>)