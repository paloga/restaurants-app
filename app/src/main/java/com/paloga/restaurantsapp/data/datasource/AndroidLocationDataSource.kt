package com.paloga.restaurantsapp.data.datasource

import android.annotation.SuppressLint
import android.content.Context
import android.location.Geocoder
import android.location.Location
import android.location.LocationManager

class AndroidLocationDataSource(private val context: Context) : LocationDataSource {

    override fun getPostCode(): String {
        val location = getLastKnowLocation()
        val geoCoder = Geocoder(context)
        val addresses = geoCoder.getFromLocation(location.latitude, location.longitude, 1)
        return addresses.firstOrNull()?.postalCode ?: ""
    }

    @SuppressLint("MissingPermission")
    private fun getLastKnowLocation(): Location {
        val locationManager = context.getSystemService(Context.LOCATION_SERVICE) as LocationManager
        return locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER)
    }

}