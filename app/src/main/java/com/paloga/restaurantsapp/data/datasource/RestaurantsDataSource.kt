package com.paloga.restaurantsapp.data.datasource

import com.paloga.restaurantsapp.data.entity.RestaurantEntity

interface RestaurantsDataSource {
    fun getRestaurantsByPostCode(postCode: String): List<RestaurantEntity>
}