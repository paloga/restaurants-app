package com.paloga.restaurantsapp.data.datasource

class RestaurantsDataFactory {
    fun create(): RestaurantsDataSource {
        return ApiRestaurantsDataSource()
    }
}