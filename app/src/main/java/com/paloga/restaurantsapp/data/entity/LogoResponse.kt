package com.paloga.restaurantsapp.data.entity

import com.google.gson.annotations.SerializedName

data class LogoResponse (@SerializedName("StandardResolutionURL") val standardResUrl: String)