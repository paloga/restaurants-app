package com.paloga.restaurantsapp.data.mapper

import com.paloga.restaurantsapp.data.entity.RestaurantEntity
import com.paloga.restaurantsapp.data.entity.RestaurantResponse

class RestaurantMapper {
    fun transform(restaurantResponse: List<RestaurantResponse>): List<RestaurantEntity> {
        return restaurantResponse.map { transform(it) }
    }

    private fun transform(restaurantResponse: RestaurantResponse) = restaurantResponse.let {
        RestaurantEntity(name = it.name, cuisineTypes = it.cuisineTypes, rating = it.rating, logo = it.logos.firstOrNull()?.standardResUrl)
    }
}