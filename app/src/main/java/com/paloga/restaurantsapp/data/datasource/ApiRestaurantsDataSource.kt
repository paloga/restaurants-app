package com.paloga.restaurantsapp.data.datasource

import com.paloga.restaurantsapp.data.JustEatApiService
import com.paloga.restaurantsapp.data.entity.RestaurantEntity
import com.paloga.restaurantsapp.data.mapper.RestaurantMapper

class ApiRestaurantsDataSource : RestaurantsDataSource {

    private val justEatApiService by lazy { JustEatApiService.create() }

    override fun getRestaurantsByPostCode(postCode: String): List<RestaurantEntity> {
        val retrofitCall = justEatApiService.getRestaurantsByOutCode(postCode)
        val result = retrofitCall.execute().body()
        val restaurants = RestaurantMapper().transform(result?.restaurants ?: emptyList())
        return restaurants
    }
}