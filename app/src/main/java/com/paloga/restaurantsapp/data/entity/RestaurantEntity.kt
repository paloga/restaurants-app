package com.paloga.restaurantsapp.data.entity

import com.paloga.restaurantsapp.domain.model.CuisineTypeReadable
import com.paloga.restaurantsapp.domain.model.RestaurantReadable

class RestaurantEntity(override val name: String,
                       override val cuisineTypes: List<CuisineTypeReadable>,
                       override val rating: Float,
                       override val logo: String?) : RestaurantReadable