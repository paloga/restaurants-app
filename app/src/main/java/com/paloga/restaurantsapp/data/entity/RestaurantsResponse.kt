package com.paloga.restaurantsapp.data.entity

import com.google.gson.annotations.SerializedName

class RestaurantsResponse(@SerializedName("Restaurants") val restaurants: List<RestaurantResponse>)