package com.paloga.restaurantsapp.data.entity

import com.google.gson.annotations.SerializedName
import com.paloga.restaurantsapp.domain.model.CuisineTypeReadable

data class CuisineTypeResponse (@SerializedName("Name") override val name: String):CuisineTypeReadable