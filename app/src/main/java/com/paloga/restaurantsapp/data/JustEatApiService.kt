package com.paloga.restaurantsapp.data

import com.paloga.restaurantsapp.BuildConfig
import com.paloga.restaurantsapp.data.entity.RestaurantsResponse
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.Query

interface JustEatApiService {

    @Headers(
            "Accept-Tenant: uk",
            "Accept-Language: en-GB",
            "Authorization: Basic VGVjaFRlc3Q6bkQ2NGxXVnZreDVw ",
            "Host: public.je-apis.com"

    )
    @GET("/restaurants")
    fun getRestaurantsByOutCode(@Query("q") outCode: String): Call<RestaurantsResponse>;

    companion object {
        fun create(): JustEatApiService {

            val client = OkHttpClient().newBuilder()
                    .addInterceptor(HttpLoggingInterceptor().apply {
                        level = if (BuildConfig.DEBUG) HttpLoggingInterceptor.Level.BODY else HttpLoggingInterceptor.Level.NONE
                    })
                    .build()

            val retrofit = Retrofit.Builder()
                    .baseUrl("https://public.je-apis.com/")
                    .client(client)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build()

            return retrofit.create(JustEatApiService::class.java)
        }
    }
}