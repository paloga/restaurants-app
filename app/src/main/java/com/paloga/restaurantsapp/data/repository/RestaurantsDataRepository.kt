package com.paloga.restaurantsapp.data.repository

import com.paloga.restaurantsapp.data.datasource.RestaurantsDataFactory
import com.paloga.restaurantsapp.domain.model.RestaurantReadable
import com.paloga.restaurantsapp.domain.repository.RestaurantsRepository

class RestaurantsDataRepository(private val restaurantsDataFactory: RestaurantsDataFactory) : RestaurantsRepository {

    override fun getRestaurantsByPostCode(postCode: String): List<RestaurantReadable> {
        return restaurantsDataFactory.create().getRestaurantsByPostCode(postCode)
    }

}