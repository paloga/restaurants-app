package com.paloga.restaurantsapp.data.datasource

interface LocationDataSource {
    fun getPostCode(): String
}