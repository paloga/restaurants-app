package com.paloga.restaurantsapp.data.mapper

import com.paloga.restaurantsapp.data.entity.RestaurantEntity
import com.paloga.restaurantsapp.domain.model.CuisineType
import com.paloga.restaurantsapp.domain.model.CuisineTypeReadable
import com.paloga.restaurantsapp.domain.model.Restaurant
import com.paloga.restaurantsapp.domain.model.RestaurantReadable

class DbDataMapper {

    fun convertFromDomain(restaurant: RestaurantReadable) = with(restaurant) {
        RestaurantEntity(name, cuisineTypes, rating, logo)
    }

    fun convertToDomain(restaurant: RestaurantReadable) = with(restaurant) {
        Restaurant(name, cuisineTypes.map { convertToDomain(it) }, rating, logo)
    }

    fun convertToDomain(cuisineType: CuisineTypeReadable) = with(cuisineType) {
        CuisineType(name)
    }
}