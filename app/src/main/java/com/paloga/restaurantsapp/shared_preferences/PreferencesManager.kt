package com.paloga.restaurantsapp.shared_preferences

internal interface PreferencesManager {

    fun saveBooleanPreference(key: String, value: Boolean)

    fun getBooleanPreference(key: String, default: Boolean = true): Boolean
}