package com.paloga.restaurantsapp.shared_preferences

import android.content.Context
import android.content.SharedPreferences
import com.paloga.restaurantsapp.shared_preferences.PreferencesManager

internal class AndroidPrefsManager(context: Context) : PreferencesManager {

    private val PREFS_FILE = "RestaurantPrefs"

    private val preferences: SharedPreferences = context.getSharedPreferences(PREFS_FILE, Context.MODE_PRIVATE)

    override fun saveBooleanPreference(key: String, value: Boolean) = preferences.edit().putBoolean(key, value).apply()

    override fun getBooleanPreference(key: String, default: Boolean): Boolean = preferences.getBoolean(key, default)
}