package com.paloga.restaurantsapp

import com.paloga.restaurantsapp.data.datasource.LocationDataSource
import com.paloga.restaurantsapp.data.repository.RestaurantsDataRepository
import com.paloga.restaurantsapp.domain.usecase.GetCurrentPostCode
import com.paloga.restaurantsapp.domain.usecase.GetRestaurants
import com.paloga.restaurantsapp.presentation.RestaurantsPresenter
import com.paloga.restaurantsapp.presentation.RestaurantsView
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class RestaurantsPresenterTest {
    @Mock
    lateinit var restaurantsView: RestaurantsView

    @Mock
    lateinit var getRestaurantsUseCase: GetRestaurants
    lateinit var getCurrentPostCodeUseCase: GetCurrentPostCode

    @Mock
    lateinit var restaurantsDataRepository: RestaurantsDataRepository
    lateinit var locationDataSource: LocationDataSource

    lateinit var restaurantsPresenter: RestaurantsPresenter

    private open class FakeLocationDataSource : LocationDataSource {
        override fun getPostCode(): String {
            return "FakePostCode"
        }
    }

    @Before
    fun setUp() {
        getRestaurantsUseCase = GetRestaurants(restaurantsDataRepository)
        locationDataSource = FakeLocationDataSource()
        getCurrentPostCodeUseCase = GetCurrentPostCode(locationDataSource)
        restaurantsPresenter = RestaurantsPresenter(getRestaurantsUseCase, getCurrentPostCodeUseCase)
    }

    @Test
    fun testStartCallsInitViews() {
        // When
        restaurantsPresenter.attachView(restaurantsView)
        restaurantsPresenter.start()

        // Then
        Mockito.verify(restaurantsView).initViews()
    }

    @Test
    fun testStartCallsShowStartView() {
        // When
        restaurantsPresenter.attachView(restaurantsView)
        restaurantsPresenter.start()

        // Then
        Mockito.verify(restaurantsView).showStartView()
    }

    @Test
    fun testStartCallsCheckLocationPermission() {
        // When
        restaurantsPresenter.attachView(restaurantsView)
        restaurantsPresenter.start()

        // Then
        Mockito.verify(restaurantsView).checkLocationPermission()
    }

    @Test
    fun testOnSearchClickedShowsErrorWhenSearchIsEmpty() {
        // When
        restaurantsPresenter.attachView(restaurantsView)
        restaurantsPresenter.onSearchClicked("")

        // Then
        Mockito.verify(restaurantsView).showPostCodeEmptyError()
    }

    @Test
    fun testOnSearchClickedShowsLoadingWhenSearchIsNotEmpty() {
        // When
        restaurantsPresenter.attachView(restaurantsView)
        restaurantsPresenter.onSearchClicked("se19")

        // Then
        Mockito.verify(restaurantsView).showLoading(false)
    }

    @Test
    fun testOnSearchClickedCallsLoadDataWhenSearchIsNotEmpty() {
        // When
        restaurantsPresenter.attachView(restaurantsView)
        restaurantsPresenter.onSearchClicked("se19")

        // Then
        Mockito.verify(restaurantsView).loadData(false)
    }
}
