
## Answers to technical questions

 - **How long did you spend on the coding test? What would you add to your solution if you had more time? If you didn't spend much time on the coding test then use this as an opportunity to explain what you would add.**
I think I have spent near 8 hours. I don't consider myself a fast developer but this weekend was a little bit difficult to save time for the test. 
I think I would have added some animations to the activity.
 - **What was the most useful feature that was added to the latest version of your chosen language? Please include a snippet of code that shows how you've used it.**
 Well, I have used Kotlin for the test. It has some really nice and modern features compared to Java, like this ones I like so much:
 
	 - Extension functions	 

	``` kotlin
	fun ImageView.loadRoundedImage(imageUrl: String) {  
    Glide.with(this.context)  
            .load(imageUrl)  
            .apply(RequestOptions.circleCropTransform().placeholder(R.drawable.ic_launcher))  
            .into(this)  
     ```
		 
	 - Common functions from FP standard libraries

	``` kotlin
	txtCuisineTypes.text = restaurant.cuisineTypes.joinToString(separator = ", ") { it.name } 
     ```
	 
	 - Coroutines & Lambdas
	 
	``` kotlin
	fun getRestaurantsByPostCode(postCode: String, onRestaurantsLoaded: (List<Restaurant>) -> Unit) {
        doAsync {
            val restaurantsList = loadRestaurants(postCode)
            uiThread { onRestaurantsLoaded.invoke(restaurantsList) }
        }
    }
     ```
 - **How would you track down a performance issue in production? Have you ever had to do this?** First of all, I would suspect from Sqlite. It's a common bottleneck in every application, independently of the platform.
 
    If it is not a sql problem, I will look at memory leaks. I know there is a leakcanary library for theses cases but I have never used it. I will try to see is there is a static context that could be leaked. 
    
    Another usual problem is having a bad performance when scrolling a recyclerview. That could be because of a bad layout design (maybe so much anidations. We can track this with a "developer option" in our device) or maybe because we are doing too much work in main thread.
    We can also use Android Studio profiler to see how much CPU or Memory is our application using.
    
 - **How would you improve the Just Eat APIs that you just used?**
    I don't really know...It maybe better to change properties naming to start with lowercase? For example: "isOpenNow" instead of "IsOpenNow"?
 - **Please describe yourself using JSON.**
    ```JSON
    {
    	"name": "Pablo",
    	"surname": "López García",
    	"height": "190",
    	"hairColor": "dark",
    	"eyeColor": "brown",
    	"birthYear": "1985",
    	"gender": "male",
    	"hobbies": [
    	    {
    			"id": 1,
    			"name": "Technology"
    		},
    		{
    			"id": 2,
    			"name": "Sports"
    		},
    		{
    			"id": 3,
    			"name": "Animals"
    		}
    	],
    	"professionalDescription": "I have been working as an Android Developer and Head of Development for more than 6 six years, trying to be updated in terms of good practices knowledge, new components and architecture patterns. I assist regularly to meetups and technology events to be up to date."
    }
```
 
